package cz.vse.java.xname.main;

public interface PredmetPozorovani {

    /**
     * pomocí metody se registrují pozorovatelé k odběru změn
     * @param pozorovatel
     */
    void registruj(Pozorovatel pozorovatel);
}
