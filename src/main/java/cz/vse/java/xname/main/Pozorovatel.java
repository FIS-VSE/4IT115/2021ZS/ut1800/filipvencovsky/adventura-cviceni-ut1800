package cz.vse.java.xname.main;

public interface Pozorovatel {
    /**
     * metoda, kterou volá předmět pozorování, když dojde ke změně
     */
    void aktualizuj();
}
