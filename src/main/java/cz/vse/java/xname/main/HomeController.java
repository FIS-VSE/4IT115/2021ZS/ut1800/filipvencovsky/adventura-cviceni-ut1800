package cz.vse.java.xname.main;

import cz.vse.java.xname.logika.Hra;
import cz.vse.java.xname.logika.IHra;
import cz.vse.java.xname.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.Map;
import java.util.SimpleTimeZone;

public class HomeController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> seznamVychodu;
    @FXML private Button tlacitkoOdesli;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    private IHra hra = new Hra();
    private Map<String, Point2D> souradniceProstoru = new HashMap<>();
    private Map<String, String> nazvySouboruIkonekProstoru = new HashMap<>();

    @FXML
    private void initialize() {
        hra.getHerniPlan().registruj(this);

        nazvySouboruIkonekProstoru.put("les","hrac.png");
        // TODO: doplnit další prostory

        seznamVychodu.setCellFactory(new Callback<ListView<Prostor>, ListCell<Prostor>>() {
            @Override
            public ListCell<Prostor> call(ListView<Prostor> param) {
                return new ListCell<Prostor>() {
                    @Override
                    protected void updateItem(Prostor prostor, boolean bln) {
                        super.updateItem(prostor, bln);
                        if (prostor != null) {

                            ImageView pohled = new ImageView("hrac.png"); //TODO: načítat názvy z mapy
                            pohled.setPreserveRatio(true);
                            pohled.setFitHeight(70);

                            setText(prostor.getNazev());
                            setGraphic(pohled);
                        } else {
                            setText("");
                            setGraphic(new ImageView());
                        }
                    }
                };
            }
        });

        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        souradniceProstoru.put("domeček", new Point2D(14,76));
        souradniceProstoru.put("les", new Point2D(84,36));
        souradniceProstoru.put("hluboký_les", new Point2D(160,83));
        souradniceProstoru.put("chaloupka", new Point2D(235,36));
        souradniceProstoru.put("jeskyně", new Point2D(165,165));

        nacteniMistnosti();
    }

    private void nacteniMistnosti() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        seznamVychodu.getItems().addAll(aktualniProstor.getVychody());

        hrac.setLayoutX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setLayoutY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());
    }

    public void zpracujVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText("> "+prikaz+"\n");
        vystup.appendText(vysledek+"\n\n");
        vstup.clear();

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            tlacitkoOdesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    public void klikSeznamVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = seznamVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor==null) return;
        String prikaz = "jdi "+novyProstor;
        zpracujPrikaz(prikaz);
    }

    @Override
    public void aktualizuj() {
        seznamVychodu.getItems().clear();
        nacteniMistnosti();
    }

    public void klikNapoveda() {

        WebView view = new WebView();
        String urlNapovedy = getClass().getResource("/napoveda.html").toString();
        view.getEngine().load(urlNapovedy);

        Stage stage = new Stage();
        stage.setTitle("Nápověda");
        stage.setScene(new Scene(view));
        stage.show();
    }
}
